from django.shortcuts import render
from .models import *
from django.http import HttpResponseRedirect
from django.shortcuts import redirect
# Create your views here.


def index(request):
    todo = Todo.objects.all()
    return render(request, 'index.html', {
        'todo': todo
    })


def create(request):
    if request.method == 'POST':
        todo_name = request.POST['todo']
        todo = Todo.objects.create(name=todo_name)
        todo.save()
        return HttpResponseRedirect('/')
        
    return render(request, 'create.html')

def detail(request, id):
    item = Todo.objects.get(id=id)
    return render(request, 'detail.html', {
        'todo': item
    })

def update(request, id):
    if request.method == "POST":
        todo_now = request.POST['todo']
        updated_todo = Todo(id=id, name=todo_now)
        updated_todo.save()

        return HttpResponseRedirect('/')

    todo = Todo.objects.get(id=id)
    return render(request, 'update.html', {
        'todo': todo
    })

def delete(request, id):
    if request.method == "POST":
        todo = Todo.objects.get(id=id)
        todo.delete()
        return redirect('/')
    return redirect('/')