from django.urls import path, include
from .views import *

urlpatterns = [
    path('', index, name="index"),
    path('create/', create, name="create"),
    path('item/<int:id>', detail, name="detail"),
    path('update/<int:id>', update, name="update"),
    path('delete/<int:id>', delete, name="delete")
]
